#-*- coding: utf-8 -*-

import random
import sqlite3

seed = 1
rand = random.Random()
rand.seed(seed)


class Game():
    def __init__(self):
        self.Board = [4,4,4,4,4,4,4,4,4,4,4,4]
        self.PointsA = 0
        self.PointsB = 0
        self.ValidMovesA = []
        self.ValidMovesB = []
        self.check_valid_moves(True)
        self.check_valid_moves(False)
        self.History = []
        self.GameFinished = False
        
        
    def check_valid_moves(self, player_a):
        vm_indexes = []
        if player_a:
            vm_indexes = [i for i,m in enumerate(self.Board[:6]) if m != 0]
        else:
            vm_indexes = [i+6 for i,m in enumerate(self.Board[6:]) if m != 0]
        result = []
        
        for i in vm_indexes:
            if self.try_move(player_a,i):
                result.append(i)

        if player_a:
            self.ValidMovesA = result
        else:
            self.ValidMovesB = result

        if len(result) == 0:
            # Es kann der Fall eintreten dass es z.b.23:23 steht. aber einer keinen Zug mehr machen kann.(Seed 5)
            # in dem Fall lautet die Regel dass die restlichen eigenen Steine als eigene Punkte gelten.
            # TODO...
            self.PointsA += sum(self.Board[:6])
            self.PointsB += sum(self.Board[6:])
            self.game_finished()
        return result
       
        
    def game_finished(self):
        #print ("Spiel beendet. {0}:{1}".format(self.PointsA,self.PointsB))
        #print (self.Board)
        self.GameFinished = True
    
    
    def Move(self,player_a,field):
        # field = in Board, index_valid_moves = index in ValidMovesA/B
        
        if player_a and field not in self.ValidMovesA or not player_a and field not in self.ValidMovesB:            
            print ("DEPRICATED - Fehler. Ungültiger Zug")
            return 
        
        if player_a:
            if len(self.ValidMovesA) == 0:
                # Fehlermeldung ist redundant? oben viel einfacher
                print ("DEPRICATED - Error kein Züge mehr für A möglich")
                return False
            if not field in self.ValidMovesA:
                print ("DEPRICATED - Kein Gültiger zug")
                return False
            
            #field = index_valid_moves
        else:
            if len(self.ValidMovesB) == 0:
                print ("DEPRICATED - Error kein Züge mehr für B möglich")
                return False
            if not field in self.ValidMovesB:
                print ("DEPRICATED - Kein Gültiger zug")
                return False

        if self.try_move(player_a,field,simulate=False):
            self.History.append(field)
        else:
            print ("DEBUG: kein Zug mehr möglich. Sonderregel für Ende!")
            # return False

            return

        if self.PointsA > 24 or self.PointsB > 24:
            self.game_finished()
    
    def try_move(self,player_a,field, simulate=True):
        
        # Erstelle ein temp Board - alles op. auf diesen. Nur wenn gültig -> self.Board
        board = self.Board.copy()
        points_a = 0
        points_b = 0
        stones = board[field]
        if stones == 0:
            print ("Fehler: Feld hat 0 Steine!")
            return False
        
        board[field] = 0
        start = field
        pos = start        
        # Verteile gegen Uhrzeigersinn 0,1,2...11
        while stones > 0:
            pos = (pos +1) % 12
            if pos != start:
                board[pos] += 1
                stones -= 1
                #print (stones)
        
        # Verteilen fertig. Schlagen für Spieler A möglich?
        while player_a and pos >= 6 and (board[pos] == 2 or board[pos] == 3):
            # Schlage Steine auf pos
            points_a += board[pos]
            board[pos] = 0
            pos = (pos-1) % 12
        # Hatt Gegner noch Steine für nächste Runde? - sonst Zug ungültig
            if sum(board[6:]) == 0:
                return False
            
        # Schlagen Spieler B
        while not player_a and pos < 6 and (board[pos] == 2 or board[pos] == 3):
            # Schlage Steine auf pos
            points_b += board[pos]
            #print (str(simulate) + " pointsb "+str(points_b))
            board[pos] = 0
            pos = (pos-1) % 12
            if sum(board[:6]) == 0:
                return False
        
        # Ein Spieler kann nächste Runde nicht mehr ziehen
        if (player_a and sum(board[6:]) == 0) or (not player_a and sum(board[:6]) == 0):
            #print ("Last false in trymove. p:{0} {1}".format(player_a,field))
            return False
        
        if not simulate:
            self.Board = board.copy()
            self.PointsA += points_a
            self.PointsB += points_b
            #print ("Player fertig. "+ str(player_a))
        return True
    

def playGame(moves=1200,seed=0):
    g = Game()
    
    for i in range(moves):
        # Endlospiel vermeiden. ab 200?
        if i > 200:
            last = len(g.History)-1
            if g.History[last-48:last-24] == g.History[last-24:last]:
                print ("Endlospiel abbruch: {0}".format(seed))
                break

        valid_moves_a = g.check_valid_moves(True)
        if g.GameFinished:
            #print ("Breaking for.. PlayerA")
            break
        index = rand.randint(0,len(valid_moves_a)-1)
        g.Move(True,valid_moves_a[index])
        
        # Spieler B:
        valid_moves_b = g.check_valid_moves(False)
        if g.GameFinished:
            #print ("Breaking for.. PlayerB")
            break
        index = rand.randint(0,len(valid_moves_b)-1)
        g.Move(False,valid_moves_b[index])
    return g



def compress_moves(moves):
    pass


def write_game_to_DB(cursor, seed,ScoreA, ScoreB,moves):
    query = "INSERT INTO spiele VALUES(?,?,?,?)"
    data = (seed,ScoreA,ScoreB,moves)    
    try:
        cursor.execute(query,data)
    except sqlite3.Error as err:
        print (err.args)   




db_con = sqlite3.connect("spiele_MC.db")
# CREATE TABLE `spiele` ( `seed` INTEGER NOT NULL UNIQUE, `scoreA` INTEGER NOT NULL, `ScoreB` INTEGER NOT NULL, `history` TEXT, PRIMARY KEY(`seed`) )
def PlayMany(start_seed=1,amount=100):    
    global rand
    global seed
    seed = start_seed
    rand.seed(start_seed)    
    cur = db_con.cursor()
    
    win_A = 0
    win_B = 0
    draw = 0
    for i in range(amount):
        g = playGame(seed=seed)
        
        #if (g.PointsA < 24 and g.PointsB < 24) and g.PointsA + g.PointsB != 48:
        #    print ("ERROR seed: {0} - Erg: {1}:{2}".format(seed,g.PointsA, g.PointsB))

            #print ("Sollte nicht sein! seed: " + str(seed))
            #print ("A: {} B:{}".format(g.PointsA,g.PointsB))
            #break
            
        if g.PointsA > g.PointsB:
            win_A+=1
        elif g.PointsA < g.PointsB:
            win_B+=1
        elif g.PointsA == g.PointsB:
            draw += 1
            
    

        write_game_to_DB(cur,seed,g.PointsA,g.PointsB,str(g.History))

            
        if i%5000 == 0:
            game_count = win_A+win_B+draw
            print ("{0} Spiele von {1} gespielt. ".format(game_count,amount))
            db_con.commit()
            
        seed += 1
        rand.seed(seed)
    db_con.commit()
    db_con.close()
    return (win_A,win_B,draw)




print (PlayMany(start_seed=1000,amount=9000))

# TODO endlosspiel vermeiden: zb:
#seed = 19834
#rand.seed(seed)
#g = playGame(3100,seed=seed)
#print (g.Board)
#print (len(g.History))
    
    
    