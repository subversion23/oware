#-*- coding: utf-8 -*-

# https://ilk.uvt.nl/icga/journal/contents/content25-3.htm#AWARI%20IS%20SOLVED

# TODO:
# - anfütterungspflicht
# -
import random

seed = 1
rand = random.Random()
rand.seed(seed)

moves = []

PointsA = 0
PointsB = 0


# Erstellt das Spielfeld und füllt 4 Steine in jede Schale:
board = []
def reset_board():
    global board
    global PointsA
    global PointsB
    PointsA = 0
    PointsB = 0
    board = []
    for i in range(12):
        board.append(4)
    #return b

def can_take(player_a,pos):
    if pos < 0 or pos > 11:
        print("FEHLER ------------------- pos: {0}".format(pos))
    if player_a:
        if pos < 6: #in FieldsA:
            return False        
    else:      
        if pos >= 6: #in FieldsB:
            return False
    if board[pos] == 2 or board[pos] == 3:
        return True
    else:
        return False
    

def make_move(player_a):    
    player = "A" if player_a else "B"
    start = None
    global PointsA
    global PointsB
    global board
    
    # Wenn kein Zug mehr möglich -> Abrechen!
    if player_a and sum(board[:6]) == 0:
        return "Kein Zug mehr für A möglich"
    if not player_a and sum(board[6:]) == 0:
        return "Kein Zug mehr für B möglich"
    
    stones = 0
    while stones == 0:
        if player_a:
            start = rand.randint(0, 5)            
        else:
            start = rand.randint(6, 11)
        # Nimm die Steine aus einem eigenen Feld außer fütterpflicht:
        # ... TODO
        stones = board[start]
        board[start] = 0
        #moves.append(start)        
    # DEBUG
    #print("Spieler {0} hat {1} Steine von Feld {2} genommen.".format(player,stones,start))            
        
    # Verteile gegen Uhrzeigersinn 0,1,2...11    
    pos = start    
    while stones > 0:
        pos = (pos + 1) % 12        
        # nur wenn nicht Startfeld
        if pos != start:            
            board[pos] += 1
            stones -= 1
    
    # Verteilen fertig. Schlagen möglich?
    while can_take(player_a,pos):        
        # Take
        if player_a:        
            PointsA += board[pos]
        else:
            PointsB += board[pos]
        board[pos] = 0
        pos = (pos-1) % 12
        print (pos, end= ",")
        #print ("? Steine bis Feld {0} gefangen.".format((pos)))        

    if check_points() != 48:
        print ("Fehler --------------")
    return("OK")

                 
def play_game(moves=10):
    for i in range(moves):        
        result = ""
        if i%2 == 0:
            result = make_move(True)
        else:
            result = make_move(False)        
        
        if PointsA > 24 or PointsB > 24:
            game_finished(PointsA, PointsB)
            return
        
        if result != "OK":
            print (result)
            print ("Spiel beendet!")
            return   
               
def game_finished(points_a,points_b):
    print ("Spiel beendet. {0}:{1}".format(points_a, points_b))

     
def check_points():
    pts = sum(board) + PointsA + PointsB
    return pts

       
reset_board()
#play_game(300)


#for game in range(30):    
#    reset_board()
#    play_game(1000)
    
    

#play(10)    
      
